# ASCII Mandelbrot

```
                                          @


                                        $
                                      @@@$
                                      @@@@
                                      @@$@
                                    #$   #
                                @  @@@@@@@@@@
                               #@@@@@@@@@@@@@@$@@
                                @@@@@@@@@@@@@@@@@
                             $ @@@@@@@@@@@@@@@@@
                              @@@@@@@@@@@@@@@@@@@
                            #$@@@@@@@@@@@@@@@@@@@*$
                             @@@@@@@@@@@@@@@@@@@@@
                   @ $@$    $@@@@@@@@@@@@@@@@@@@@@
                   $@@@@@#  @@@@@@@@@@@@@@@@@@@@@@
                  @@@@@@@@* @@@@@@@@@@@@@@@@@@@@@@
                  @@@@@@@@$ @@@@@@@@@@@@@@@@@@@@@@
               $@ @@@@@@@@@ @@@@@@@@@@@@@@@@@@@@@
  @$@@:@*$~#*@$@@@@@@@@@@@@@@@@@@@@@@@@@@@@@$$#
               $@ @@@@@@@@@ @@@@@@@@@@@@@@@@@@@@@
                  @@@@@@@@$ @@@@@@@@@@@@@@@@@@@@@@
                  @@@@@@@@* @@@@@@@@@@@@@@@@@@@@@@
                   $@@@@@#  @@@@@@@@@@@@@@@@@@@@@@
                   @ $@$    $@@@@@@@@@@@@@@@@@@@@@
                             @@@@@@@@@@@@@@@@@@@@@
                            #$@@@@@@@@@@@@@@@@@@@*$
                              @@@@@@@@@@@@@@@@@@@
                             $ @@@@@@@@@@@@@@@@@
                                @@@@@@@@@@@@@@@@@
                               #@@@@@@@@@@@@@@$@@
                                @  @@@@@@@@@@
                                    #$   #
                                      @@$@
                                      @@@@
                                      @@@$
                                        $


                                          @

```

Based on definition of the classic [Mandelbrot Set](https://en.wikipedia.org/wiki/Mandelbrot_set)

## Build

`gcc -lm -o mandel mandel.c`

## Run

* Run `./mandel`

## Usage

| Keypress | Action     |
| -------- | ---------- |
| q        | Quit       |
| h        | Move left  |
| j        | Move down  |
| k        | Move up    |
| l        | Move right |
| +        | Zoom in    |
| -        | Zoom out   |
| r        | Reset view |
